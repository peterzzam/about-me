# About me



## Description

This is my about-me page.

In this project I aim to implement various technologies from the DevOps stack.

## Technology stack

Languages:
- [ ] Python

Contenerization:
- [ ] Docker
- [ ] Kubernetes
- [ ] Helm

DB:
- [ ] MySQL

IaC:
- [ ] Terraform

Cloud-specific:
- [ ] AWS Secrets Manager
- [ ] AWS IAM
- [ ] AWS EC2

Web server:
- [ ] Nginx

CI/CD:
- [ ] GitLab CI

Infrastructure and application monitoring:
- [ ] DataDog
- [ ] Open Telemetry

Additonal networking features to implement / topics to explore:
- [ ] Caching
- [ ] Proxy
- [ ] Firewall

## Running the app locally

py -3 -m venv development

Set-ExecutionPolicy RemoteSigned

.\development\Scripts\activate 

Set-ExecutionPolicy Restricted

pip install python-dotenv

pip install flask

flask run

## Running the app using Docker

docker build --tag about-me .

docker run -d -p 8000:8000 about-me