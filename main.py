from flask import Flask, render_template
import sys
import os


app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')

@app.route('/privacy')
def privacy():
    return 'This is a placeholder for your privacy policy'

@app.route('/shutdown')
def shutdown():
    sys.exit()
    os.exit(0)
    return

if (__name__ == '__main__'):
    app.run()